package server;

import Algorithms.BruteForce;
import Algorithms.Greedy;
import Algorithms.RandomSearch;
import App.Knapsack;
import api.AppInterface;
import api.ServerInfo;

import java.rmi.ConnectException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {
    public static void main(String[] args) throws RemoteException {
        if (args.length < 2) {
            System.err.println("There have to be 2 arguments: name algorithmName(BruteForce|Greedy|RandomSearch)");
            System.exit(1);
        }

        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new RMISecurityManager());
        }

        String name = args[0];
        String algorithm = args[1];

        Registry registry = null,
                appRegistry = null;

        appRegistry = LocateRegistry.getRegistry(1010);

        try {
            registry = LocateRegistry.createRegistry(1011);
        } catch (RemoteException e) {
            registry = LocateRegistry.getRegistry(1011);
        }

        Knapsack alg = null;

        if(((alg = parseAlgorithm(algorithm)) == null))
        {
            System.err.println("Invalid algorithm name (Valid names BruteForce, Greedy, RandomSearch)");
            System.exit(1);
        }

        try {
            AppInterface servers = (AppInterface) appRegistry.lookup("serverList");

            if(servers.register(new ServerInfo(name, algorithm, alg.description()))) {
                registry.rebind(name, new ServerImplementation(alg));
                System.out.println(name + " is working");
            }
        } catch (ConnectException e) {
            System.err.println("ServerList app is no working");
            System.exit(1);
        } catch (Exception e) {
            System.err.println(name + " error: " + e.getMessage());
        }
    }

    private static Knapsack parseAlgorithm(String name) {
        if(name.equals("BruteForce")) {
            return new BruteForce();
        }
        else if(name.equals("Greedy")) {
            return new Greedy();
        }
        else if(name.equals("RandomSearch")) {
            return new RandomSearch();
        }

        return null;
    }
}
