package server;

import App.Instance;
import App.Knapsack;
import App.Solution;
import api.ServerInterface;

import java.rmi.RemoteException;
import java.rmi.server.RMIClientSocketFactory;
import java.rmi.server.RMIServerSocketFactory;
import java.rmi.server.UnicastRemoteObject;

public class ServerImplementation extends UnicastRemoteObject implements ServerInterface {
    private Knapsack algorithm = null;

    public ServerImplementation() throws RemoteException {
    }

    public ServerImplementation(Knapsack algorithm) throws RemoteException {
        this.algorithm = algorithm;
    }

    protected ServerImplementation(int port) throws RemoteException {
        super(port);
    }

    protected ServerImplementation(int port, RMIClientSocketFactory csf, RMIServerSocketFactory ssf) throws RemoteException {
        super(port, csf, ssf);
    }

    @Override
    public Solution solve(Instance instance) throws RemoteException {
        if(algorithm != null) {
            algorithm.setInstance(instance);

            System.out.println("[LOG] Solving problem. Algorithm: " + algorithm.name() + ", Instance: " + instance.getItemList().size() + " items, knapsack size + " + instance.getBackpackSize());

            return algorithm.findSolution();
        }

        return null;
    }
}
