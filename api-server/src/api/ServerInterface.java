package api;

import App.Instance;
import App.Solution;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ServerInterface extends Remote {
    Solution solve(Instance instance) throws RemoteException;
}
