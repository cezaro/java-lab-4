package client;

import Algorithms.BruteForce;
import Algorithms.Greedy;
import Algorithms.RandomSearch;
import App.Instance;
import App.Item;
import App.Knapsack;
import App.Solution;
import api.AppInterface;
import api.ServerInfo;
import api.ServerInterface;
import javafx.application.Platform;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.net.URL;
import java.rmi.ConnectException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML private VBox root;

    @FXML private TableView<Item> itemsTable;

    @FXML private TableColumn<Item, Integer> columnIndex;
    @FXML private TableColumn<Item, Integer> columnWeight;
    @FXML private TableColumn<Item, String> columnValue;
    @FXML private TableColumn<Item, String> columnRatio;

    @FXML private TextField weightInput;
    @FXML private TextField valueInput;
    @FXML private TextField knapsackSizeInput;

    @FXML private Label descriptionLabel;

    @FXML private Button addItemButton;
    @FXML private Button findSolutionButton;

    @FXML private ComboBox algorithmSelect;

    @FXML private Text itemsCount;
    @FXML private Text solutionText;

    private ArrayList<Item> itemList = new ArrayList<Item>();
    private Solution solution = null;

    private Locale locale = Locale.getDefault();

    /** RMI Variables */
    private ServerInterface serverApi;
    private AppInterface serversApi;

    private ArrayList<ServerInfo> servers = new ArrayList<>();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        refreshServers();

        columnIndex.setCellValueFactory(cellData -> {
            Item item = cellData.getValue();

            return new SimpleIntegerProperty(itemList.indexOf(item) + 1).asObject();
        });

        columnWeight.setCellValueFactory(new PropertyValueFactory<Item, Integer>("weight"));
        columnValue.setCellValueFactory(cellData -> {
            Item item = cellData.getValue();

            return new SimpleStringProperty(String.format(locale,"%.2f", item.getValue()));
        });

        columnRatio.setCellValueFactory(cellData -> {
            Item item = cellData.getValue();

            return new SimpleStringProperty(String.format(locale,"%.3f",item.getRatio()));

        });

        updateSolutionText();
        updateTable();

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                setClasses(false);
            }
        });

        algorithmSelect.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue value, String oldValue, String newValue) {
                updateDescription();
            }
        });

        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new RMISecurityManager());
        }
    }

    public boolean addItemButton(ActionEvent actionEvent) {
        if (weightInput.getText().equals("") || valueInput.getText().equals("")) {
            return false;
        }

        Integer weight = Integer.valueOf(weightInput.getText());
        Float value = Float.valueOf(valueInput.getText().replace(",", "."));

        if (!this.addItem(weight, value)) {
            return false;
        }

        valueInput.setText("");
        weightInput.setText("");

        return true;
    }

    public boolean findSolutionEvent(ActionEvent actionEvent) throws RemoteException {
        ServerInfo actualServer = null;

        if((actualServer = getServer()) == null) {
            return false;
        }

        String knapsackSize = knapsackSizeInput.getText();

        if (knapsackSize.equals("")) {
            return false;
        }

        this.setClasses(true);

        Instance instance = new Instance(itemList, Integer.valueOf(knapsackSize));

        try {
            Registry registry = LocateRegistry.getRegistry(1011);

            serverApi = (ServerInterface) registry.lookup(actualServer.getName());
            solution = serverApi.solve(instance);
        } catch (Exception e) {
            System.err.println("RMI Error: " + e.getMessage());
        }

        updateSolutionText();

        addItemButton.setDisable(true);
        this.setClasses(false);

        return true;
    }

    public void closeAplication(ActionEvent actionEvent) {
        System.exit(0);
    }

    public void clearForm(ActionEvent actionEvent) {
        solution = null;
        this.setClasses(true);

        itemList.clear();
        updateSolutionText();
        updateTable();
        knapsackSizeInput.clear();

        addItemButton.setDisable(false);
    }

    public void insertTestData(ActionEvent actionEvent) {
        knapsackSizeInput.setText("15");

        itemList.add(new Item(1, 2));
        itemList.add(new Item(12, 4));
        itemList.add(new Item(4, 10));
        itemList.add(new Item(1, 1));
        itemList.add(new Item(2, 2));

        updateTable();
    }

    public void insertRandomData(ActionEvent actionEvent) {
        Instance i = Instance.generate(null, null);

        itemList = i.getItemList();
        knapsackSizeInput.setText(String.valueOf(i.getBackpackSize()));
        updateTable();
    }

    private void setClasses(boolean clearClasses) {
        int i = 0;

        for (Node n : itemsTable.lookupAll("TableRow")) {
            if (n instanceof TableRow && i < itemList.size()) {
                TableRow row = (TableRow) n;

                Item item = itemsTable.getItems().get(i);

                if (clearClasses) {
                    row.getStyleClass().clear();
                } else if (isInSolution(item))
                    row.getStyleClass().add("selected");

                i++;
            }
        }
    }

    private boolean addItem(Integer weight, Float value) {
        Item item = new Item(weight, value);

        if (itemList.indexOf(item) != -1) {
            return false;
        }

        itemList.add(item);
        updateTable();

        return true;
    }

    private boolean isInSolution(Item item) {
        return solution != null && solution.getItemList().indexOf(item) != -1 ? true : false;
    }

    private ServerInfo getServer() {
        int index = algorithmSelect.getSelectionModel().getSelectedIndex();

        if(servers.size() - 1 <= index)
        {
            return servers.get(index);
        }

        return null;
    }

    private Knapsack getAlgorithm() {
        Knapsack algorithm = null;
        if(algorithmSelect.getValue() != null) {
            String algorithmName = algorithmSelect.getValue().toString();

            if (algorithmName.indexOf("BruteForce") != -1) {
                algorithm = new BruteForce();
            } else if (algorithmName.indexOf("Greedy") != -1) {
                algorithm = new Greedy();
            } else if (algorithmName.indexOf("RandomSearch") != -1) {
                algorithm = new RandomSearch();
            }
        }

        return algorithm;
    }

    private void updateTable() {
        ObservableList<Item> observableList = FXCollections.observableList(itemList);
        itemsTable.setItems(observableList);

        updateItemsCount();
    }

    private void updateItemsCount() {
        itemsCount.setText(itemList.size() + " " + getCountText(itemList.size()));
    }

    private void updateSolutionText() {
        if (solution != null) {
            Integer items = solution.getItemList().size();
            solutionText.setText("Rozwiązanie: " + items + " " + getCountText(items) + "\tWaga: " + solution.getItemsWeight() + "\tWartość: " + solution.getItemsValue());
        } else {
            solutionText.setText("");
        }
    }

    private String getCountText(Integer count) {
        String itemsString = "przedmiot|przedmioty|przedmiotów",
                correctString = null;

        String[] parts = itemsString.split("[|]");

        if(count == 1)
            correctString = parts[0];
        else if(count < 5 && count > 1)
            correctString = parts[1];
        else
            correctString = parts[2];

        return correctString;
    }

    private void updateDescription() {
        if(getAlgorithm() == null) {
            descriptionLabel.setText("");
        } else {
            descriptionLabel.setText(getAlgorithm().description());
        }
    }

    public void refreshServers() {
        try {
            Registry registry = LocateRegistry.getRegistry(1010);

            serversApi = (AppInterface) registry.lookup("serverList");
            servers = serversApi.getServers();

            algorithmSelect.getItems().clear();

            int index = 0;
            for(ServerInfo s : servers) {
                algorithmSelect.getItems().add(index++,s.getName() + " (" + s.getAlgorithmName() + ")");
            }

            if(servers.size() > 0) {
                algorithmSelect.setValue(algorithmSelect.getItems().get(0));
                algorithmSelect.setDisable(false);
                findSolutionButton.setDisable(false);
            } else {
                algorithmSelect.setValue("Brak serwerów");
                algorithmSelect.setDisable(true);
                findSolutionButton.setDisable(true);
            }

            updateDescription();
        } catch (ConnectException e) {
            System.err.println("ServerList app is no working");
            updateDescription();
            algorithmSelect.setValue("Brak serwerów");
            algorithmSelect.setDisable(true);
            findSolutionButton.setDisable(true);
        } catch (Exception e) {
            System.err.println("RMI Error: " + e.getMessage());
        }
    }
}
