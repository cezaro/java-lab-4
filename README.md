# Laboratorium 4 - Zdalne wywoływanie metod (RMI)
Aplikacja pozwalająca rozwiązać problem plecakowy, wykorzystując bibliotekę z pierwszego etapu. Dodatkowo użyto JavaFX do stworzenia aplikacji klienta. 

## Aplikacja
Jest to aplikacja serwera przechowująca listę dostępnych serwerów. Serwer po uruchomieniu rejestruje się na liście. Klient może pobrać listę dostępnych serwerów.

## Serwer
Serwer umożliwia rozwiązanie instancji problemu plecakowego przekazanego jako parametr funkcji `Solution solve(Instance instance)`. 

Podczas uruchamiania serwera należy podać 2 parametry: nazwę serwera oraz algorytm jaki serwer ma używać.

## Klient
Aplikacja z wykorzystaniem JavaFX, która przez RMI umożliwia rozwiązanie problemu plecakowego. Aplikacja umożliwia również wygenerowanie losowych danych oraz wprowadzenie danych z wiki. 