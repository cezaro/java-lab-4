package api;

import java.io.Serializable;
import java.util.Objects;

public class ServerInfo implements Serializable{
    private static final long serialVersionUID = -2498765022904859151L;

    private String name;
    private String algorithmName;
    private String algorithmDescription;

    public ServerInfo(String name, String algorithmName, String algorithmDescription) {
        this.name = name;
        this.algorithmName = algorithmName;
        this.algorithmDescription = algorithmDescription;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlgorithmName() {
        return algorithmName;
    }

    public void setAlgorithmName(String algorithmName) {
        this.algorithmName = algorithmName;
    }

    public String getAlgorithmDescription() {
        return algorithmDescription;
    }

    public void setAlgorithmDescription(String algorithmDescription) {
        this.algorithmDescription = algorithmDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServerInfo that = (ServerInfo) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
