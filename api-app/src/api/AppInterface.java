package api;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface AppInterface extends Remote {
    boolean register(ServerInfo serverInfo) throws RemoteException;
    ArrayList<ServerInfo> getServers() throws RemoteException;
}
