package app;

import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class App {

    public static void main(String[] args) throws RemoteException {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new RMISecurityManager());
        }

        Registry registry = LocateRegistry.createRegistry(1010);

        try {
            registry.rebind("serverList", new AppImplementation());
            System.out.println("ServerList is working");
        } catch (Exception e) {
            System.err.println("ServerList Error: " + e.getMessage());
        }
    }
}
