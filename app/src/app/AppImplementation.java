package app;

import api.AppInterface;
import api.ServerInfo;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.RMIClientSocketFactory;
import java.rmi.server.RMIServerSocketFactory;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class AppImplementation extends UnicastRemoteObject implements AppInterface {
    private ArrayList<ServerInfo> servers = new ArrayList<>();

    protected AppImplementation() throws RemoteException {
    }

    protected AppImplementation(int port) throws RemoteException {
        super(port);
    }

    protected AppImplementation(int port, RMIClientSocketFactory csf, RMIServerSocketFactory ssf) throws RemoteException {
        super(port, csf, ssf);
    }

    @Override
    public boolean register(ServerInfo serverInfo) throws RemoteException {
        if(servers.add(serverInfo)) {
            System.out.println("[LOG] " + serverInfo.getName() + " server registered (Servers count: " + servers.size() + ")");
            return true;
        }

        return false;
    }

    @Override
    public ArrayList<ServerInfo> getServers() throws RemoteException {
        System.out.println("[LOG] Getting servers list");
        return servers;
    }
}
